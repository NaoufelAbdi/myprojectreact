import { FiLock } from 'react-icons/fi';
import {FaFileInvoice} from 'react-icons/fa'
import { AiOutlinePoweroff , AiOutlineUser } from 'react-icons/ai';

const AccountOptionsData = [
  { name: 'Personal Details' , icons: FaFileInvoice },
  { name: 'Past Orders' , icons : FiLock },
  { name: 'Change Password' , icons: AiOutlineUser },
  { name: 'Sign Out' , icons : AiOutlinePoweroff},
]

export default AccountOptionsData
