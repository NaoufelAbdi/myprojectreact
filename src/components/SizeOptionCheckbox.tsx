import React from 'react'
import TickIcon from '../assets/icons/tick.png'
import Option from '../interfaces/Option'

interface CheckboxProps {   /* */
  onToggle(option?: any): void
  option: Option
  label?: string,
  price?: number,
  name?: string
}

// const SizeOptionCheckbox = ({active ,onToggle, label, option, price }: CheckboxProps): JSX.Element => (
//    <div className={'product-option-checkbox'}
//         onClick={() => {
//           onToggle(active)
//         }}>
//      <div>
//        <div className={`checkbox ${ active ? 'active' : 'non-active' }`}>
//          {active &&
//          <img
//            src={TickIcon}
//            alt="tick option"
//            className={'tick-icon'}
//          />}
//        </div>
//        <p>{label}</p>
//      </div>
//      {price && (
//        <p>+${price}.00</p>
//      )}
//    </div>
//   )
const SizeOptionCheckbox = ({ onToggle, option, label, price, name }: CheckboxProps): JSX.Element => (
  <>
    <div className={'product-option-checkbox'} onClick={() => onToggle(option)}> {/*rendre un bouton qui prend en argument l'element selectionné*/ }
      <input type="radio" id={option.name} name={name} value={option.name}  />
      <label htmlFor="huey" className="lala">{label}</label> {/*le label qui etre récuperer de la base de donnée en fontion du nom de "Itemoption.itemoptiongroup.choice"*/}
    </div>    
    <div className={'cc'} > {/*Le prix de chaque choix des options des produits*/}
      {price && 
        <p>${price}.00</p>
      }
    </div>
  </>
)

 export default SizeOptionCheckbox /*on export ou retourne notre model pour l'utiliser lors des chois des options des produits*/
