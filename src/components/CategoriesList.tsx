import React from 'react'
import CloseTag from './CloseTag'
import LogoImage from '../assets/images/logo.png'
import { connect } from 'react-redux'
import { Category } from '../redux/products/types'
import { Link} from 'react-scroll'

interface Props {
  data?: Category[]
  onCategoryPress(category: Category): void
  slideVisible?: boolean
  onSlideToggle?(): void
}

class CategoriesList extends React.Component<Props> {
  render(): React.ReactNode {
    let categories = this.props.data || []
    const { onCategoryPress, slideVisible, onSlideToggle } = this.props
    return (
      <div style={{height: '660px', position:'fixed'}} className={`categories-list ${slideVisible ? 'open' : ''}`}>
        {slideVisible && (
          <>
            <img src={LogoImage} alt="" className={'slide-logo'} />
            <CloseTag onClose={onSlideToggle} />
          </>
        )}
        <ul>
          {categories.map((category, index) => (
            <>
            <li key={category.id} ref={this[category.id]}>
              <Link
                activeClass="activeCategoryLink"
                className={category.name}
                to={category.id.toString()}
                spy={true}
                smooth={true}
                duration={300}
                offset={-200}
                delay={100}
              >
              <button onClick={() => onCategoryPress(category)}>
                  {category.name}
              </button>
                {/*<div className={"nav-indecator"}></div>*/}
              </Link>
            </li>

            </>
          ))}
        </ul>
      </div>
    )
  }
}

const mapStateToProps = (state: any): any => {
  return {
    data: state.products.category,
  }
}

export default connect(mapStateToProps, null)(CategoriesList)