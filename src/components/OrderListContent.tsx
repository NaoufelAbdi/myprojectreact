import React from 'react'
import OrderIcon from '../assets/icons/orderlistcc.png'
import HorizontalLine from './HorizontalLine'
import CouponIcon from '../assets/icons/coupon.png'
import { connect } from 'react-redux'
import { CartItem } from '../redux/cart/types'
import MinusIcon from '../assets/icons/minus-primary.png'
import PlusIcon from '../assets/icons/plus-primary.png'
import CashIcon from '../assets/icons/paimentcash.png'
import CrtIcon from '../assets/icons/paiementiconcarte.png'
import shopIcon from '../assets/icons/iconn.png'
import ReceiptIcon from '../assets/icons/receipt-primary.png'
import DustbinIcon from '../assets/icons/dustbin-primary.png'
import { removeProductFromCart, updateProductInCart } from '../redux/cart/actions'
import { Product } from '../redux/products/types'
import { total } from '../redux/cart/types'
import { grandTotalAction } from '../redux/cart/actions'
import { withTranslation, WithTranslation } from "react-i18next";

interface Props extends WithTranslation {
  cart?: any
  isMobile?: boolean
  onSlideToggle?(): void
  auth?: any
  onAlsoLikeClick?(): void
  GrandTotalSave?(totalData: total): void
  reOrder?: any
  onReceiptClick(product: Product): void
  updateProductInCart?(cartItem: CartItem): void
  removeProductFromCart?(cartItem: CartItem): void
}
class OrderListContent extends React.Component<Props> {
  state = {
    quantity: 1,
    total: 0,
    GrandTotal: 0,
    fees: 5
  }

  increaseQuantity = (cartItem: CartItem) => {
    if (this.props.updateProductInCart)
      this.props.updateProductInCart({ ...cartItem, quantity: cartItem.quantity + 1 })
  }

  decreaseQuantity = (cartItem: CartItem) => {
    if (this.props.updateProductInCart && cartItem.quantity > 1)
      this.props.updateProductInCart({ ...cartItem, quantity: cartItem.quantity - 1 })
  }

  receiptItem = (cartItem: CartItem) => {
    this.props.onReceiptClick(cartItem.product)
  }

  deleteItem = (cartItem: CartItem) => {
    if (this.props.removeProductFromCart) {
      this.props.removeProductFromCart(cartItem)
    }
  }
  handleCheckOut = () => {
    this.props.onAlsoLikeClick();
  }
  render(): React.ReactNode {
    const { t } = this.props
    let { cart } = this.props
    let grandTotal = cart.cart.map((cartItem: CartItem) => {
      return cartItem.quantity * (cartItem.product.price + cartItem.totalPrice)
    })
    let GrandTotal = this.state.fees;
    for (let i = 0; i < grandTotal.length; i++) {
      GrandTotal += grandTotal[i]
    }
    // const data = {product : this.props.reOrder.items[0]}
    // console.log("dat a is  resorder =>>" , data)
    // if (data.product.length !== 0){
    //   cart.cart.push(data)
    // }
    return (
      <div className={'order-content'}>
        <h4>{t('YOUR ORDER')}</h4>
        <img src={shopIcon} alt="Cart icon" className={'smoody'}/>
        {cart.cart.length > 0 ? (
          <>
            <div className={'items'}>
              {cart.cart.map((cartItem: CartItem) => (
                <div className={'details'}>
                  <div className={'edit'}>
                    <div className={'quantity'}>
                      <img
                        src={MinusIcon}
                        alt="Plus Amount"
                        className={'plus-minus-icon'}
                        onClick={() => this.decreaseQuantity(cartItem)}
                      />
                      <img
                        src={PlusIcon}
                        alt="Minus Amount"
                        className={'plus-minus-icon'}
                        onClick={() => this.increaseQuantity(cartItem)}
                      />
                    </div>
                    <div className={'right'}>
                      <img
                        src={ReceiptIcon}
                        alt="Minus Amount"
                        onClick={() => this.receiptItem(cartItem)}
                      />
                      <img
                        src={DustbinIcon}
                        alt="Delete"
                        onClick={() => this.deleteItem(cartItem)}
                      />
                    </div>
                  </div>
                  <div className={'product'}>
                    <div className={'item'}>
                      <p>{cartItem.quantity} x {cartItem.product.itemName}</p>
                      <p>{cartItem.quantity * (cartItem.product.price + cartItem.totalPrice)}</p>
                    </div>
                    <div className={'item-details'}>
                      {cartItem.size && (
                        <div>
                          <p>{cartItem.size.name}</p>
                          <p>+${cartItem.size.price}</p>
                        </div>
                      )}
                      {cartItem.extraIngredients && cartItem.extraIngredients.map((option) => (
                        <div>
                          <p>{option.name}</p>
                          <p>+${option.price}</p>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              ))}</div>
            <HorizontalLine />
            <div className={'fees'}>
              <p>{t('Fees & Discounts')}</p>
              <p>${this.state.fees}.00</p>
            </div>
            <div className={'coupon-bar'}>
              <img src={CouponIcon} alt={'Coupon icon'} />
              <input type="text" placeholder={t('ENTER COUPON CODE')} />
              <div className={'apply'}>
                <p>{t('APPLY')}</p>
              </div>
            </div>
            <HorizontalLine />
            <div className={'total'}>
              <h2>{t('Total')}</h2>
              <h2>${GrandTotal.toFixed(2)}</h2>
            </div>
            {this.props.onAlsoLikeClick && (
              <div className={'checkout-button'} onClick={this.handleCheckOut}>
                <button>
                  {t('Checkout')}
                </button>
              </div>
            )}
            <div className={'payment-method'}>
              <p>{t('We accept payment by')}</p>
              <div>
                <img  src={CashIcon} alt={'cash icon'} className={'cash'}/>
                <img  src={CrtIcon} alt={'cart icon'} className={'cart'} />
              </div>
            </div>
          </>
        ) : (
            <div className={'empty'}>
              <img src={OrderIcon} alt="Empty Order Icon" className={'empty-order'} />
              <p>{t('What you order will appear here')}</p>
            </div>
          )}
      </div>
    )
  }
}

const comp = connect((state: any) => {
  return {
    cart: state.cart,
    auth: state.auth,
    reOrder: state.order.reOrder
  };
}, (dispatch: any) => {
  return {
    updateProductInCart: (cartItem: CartItem) => dispatch(updateProductInCart(cartItem)),
    removeProductFromCart: (cartItem: CartItem) => dispatch(removeProductFromCart(cartItem.id || 0)),
    GrandTotalSave: (totalData: total) => dispatch(grandTotalAction(totalData)),
  };
})(OrderListContent);

export default withTranslation()(comp);
