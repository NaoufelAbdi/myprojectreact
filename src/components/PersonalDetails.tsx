import HorizontalLine from './HorizontalLine'
import PersonIcon from '../assets/icons/person.png'
import EmailIcon from '../assets/icons/email-2.png'
import React from 'react'
import PhoneIcon from '../assets/icons/phone.png'
import { connect } from 'react-redux'
import {AiOutlineForm} from 'react-icons/ai'
import { AiOutlineMail, AiOutlineUser } from 'react-icons/ai';
import {FiPhone} from 'react-icons/fi'
import { withTranslation, WithTranslation } from 'react-i18next'
import Loader from 'react-loader-spinner'
import { UpdateInterface } from '../redux/user/types'
import { userUpdate } from '../redux/user/actions'

interface Props extends WithTranslation {
  auth?: any
  user?: any
  updateRequest: (updateInfo: UpdateInterface) => void
  onChangeUpdate: boolean,
}
interface State {
  updateState: boolean
  LastName: string
  FirstName: string
  Phone: string
  formErrorMessage: string
}
class PersonalDetails extends React.Component<Props, State>{
  constructor(props: any) {
    super(props);
    this.state = {
      updateState: false,
      LastName: this.props.auth.lastName,
      FirstName: this.props.auth.firstName,
      Phone: "",
      formErrorMessage: ""
    }
  }
  componentWillReceiveProps(props: any) {
    if (props.user.updateUser && this.props.user.updateUser !== props.user.updateUser) {
      this.setState({ updateState: false })
    }
  }
  onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [e.target.name]: e.target.value,
      formErrorMessage: ""
    } as Pick<State, any>)
  }
  handleUpdateState = () => {
    this.setState({
      updateState: true
    })
  }
  handleCancel = () => {
    this.setState({
      updateState: false
    })
  }
  userUpdateChangeHandler = () => {
    const {FirstName , LastName ,} = this.state
    const { activated, authorities, createdBy, reatedDate, email, id, imageUrl, langKey, lastModifiedBy, lastModifiedDate, login } = this.props.auth
    if (FirstName === '') {
      this.setState({ formErrorMessage: 'Please fill the current password' })
    } else if (LastName === '') {
      this.setState({ formErrorMessage: 'Please fill the new password' })
    } else {
      const updateInfo = {
        firstName : FirstName,
        lastName: LastName,
        activated,
        authorities,
        createdBy,
        reatedDate,
        email,
        id,
        imageUrl,
        langKey,
        lastModifiedBy,
        lastModifiedDate,
        login
      }
      this.props.updateRequest(updateInfo)
    }
  }
  render() {
    const { t } = this.props
    const { firstName, lastName, email, } = this.props.auth
    const {loading} = this.props.user
    const {Phone, FirstName , LastName} = this.state
    return (
      <div className={'card detail'}>
        {this.state.updateState ? <> <div className={'formIconUpdateDiv'}><h4>{this.props.t('YOUR PERSONAL INFORMATION')}</h4></div>
            <HorizontalLine/>
            <div className={'personal-details'}>
              <div className={'userPersonalInfo'}>
                <AiOutlineUser className={"userUpdateIcon"}/>
                  <p className={"userUpdateP"}>First Name*</p>
                  <input className={"userUpdateInput"} type={"text"} placeholder={this.state.FirstName} name="FirstName"  value={FirstName} onChange={this.onChangeHandler}/>
              </div>
              <div className={'userPersonalInfo'}>
                <AiOutlineUser className={"userUpdateIcon"}/>
                <p className={"userUpdateP"}>Last Name*</p>
                <input className={"userUpdateInput"} type={"tex"} placeholder={this.state.LastName} name="LastName" value={LastName} onChange={this.onChangeHandler}/>
              </div>
            </div>
            <div className={'personal-details'}>
              <div className={'userPersonalInfo'}>
                <AiOutlineMail className={"userUpdateIcon"}/>
                <p className={"userUpdateP"}>Email (Read only)</p>
                <input className={"userUpdateInput"} type={"text"} placeholder={email} name="email" disabled />
              </div>
              <div className={'userPersonalInfo'}>
                <FiPhone className={"userUpdateIcon"}/>
                <p className={"userUpdateP"}>Phone*</p>
                <input className={"userUpdateInput"} type={"text"} placeholder={this.state.Phone} name="Phone" value={Phone} onChange={this.onChangeHandler}/>
              </div>
            </div>
          <div className={'personal-details'}>
            <div className={'userPersonalInfo'}>
              <button onClick={this.userUpdateChangeHandler} className={"userUpdateSubmitBtn"}>
                {loading ? <div><Loader
                type="Oval"
                color="white"
                height={18}
                width={18}
                timeout={700000}
              /></div> :
                <>Submit</>
                }
              </button>
              <button onClick={this.handleCancel} className={"userUpdateCancelBtn"}>
                  Cancel
              </button>
            </div>
          </div>
        </>: <div>
            <div className={'formIconUpdateDiv'}><h4>{this.props.t('YOUR PERSONAL INFORMATION')}</h4><AiOutlineForm
              className={'formIconUpdate'} onClick={this.handleUpdateState}/></div>
            <HorizontalLine/>
            <div className={'personal-details'}>
              <div className={'info-block'}>
                <img src={PersonIcon} alt={'person icon'}/>
                <div>
                  <p>{t('First Name')}</p>
                  <h5>{firstName ? firstName : "no first name"}</h5>
                </div>
              </div>
              <div className={'info-block'}>
                <img src={PersonIcon} alt={'person icon'}/>
                <div>
                  <p>{t('Last Name')}</p>
                  <h5>{lastName ? lastName : "no last name"}</h5>
                </div>
              </div>
            </div>
            <div className={'personal-details'}>
              <div className={'info-block'}>
                <img src={EmailIcon} alt={'email icon'}/>
                <div>
                  <p>{t('Email')}</p>
                  <h5>{email ? email : "no email"}</h5>
                </div>
              </div>
              <div className={'info-block'}>
                <img src={PhoneIcon} alt={'phone icon'}/>
                <div>
                  <p>{t('Phone')}</p>
                  <h5>{this.state.Phone}</h5>
                </div>
              </div>
            </div>
          </div>
        }
      </div>)
  }
}

const comp = connect((state: any) => {
  return {
    auth: state.auth.userInfo,
    user: state.auth
  };
}, (dispatch: any) => {
  return {
    updateRequest: (update: UpdateInterface) => dispatch(userUpdate(update)),
  };
})(PersonalDetails);

export default withTranslation()(comp)