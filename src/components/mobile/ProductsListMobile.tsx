import React from 'react'
import RightIcon from '../../assets/icons/right-icon.png'
import { connect } from 'react-redux'
import { Category, Product } from '../../redux/products/types'

interface Props {
  onClick(product: Product): void
  onBack(): void
  category?: Category
  data?: Category[]
}

class ProductsList extends React.Component<Props> {
  render(): React.ReactNode {
    const {
      onClick,
      category,
      onBack,
    } = this.props
    if (!category) return <div/>
    return (
      <div className={'products products-mobile'}>
        <div className={'category-header'}
             style={{ backgroundImage: `url(data:${category.imageContentType};base64,${category.image})` }}>
          <div className={'left-btn-container'}>
            <div className={'back-btn'} onClick={onBack}>
              <img src={RightIcon}/>
            </div>
          </div>
          <p>{category.name}</p>
        </div>

        {category.items.map((product, index) => (
          <div className={'product-horizontal'} key={index} onClick={() => onClick(product)}>
            <div className={'product-item'}>
              <img
                src={`data:${product.imageContentType};base64,${product.image}`}
                alt=""
                sizes={'700vm'}
                className={'product-image'}
              />
              <div className={'product-info'}>
                <h4 className={'name'}>{product.itemName}</h4>
                <p className={'description'}>
                  {product.ingredient}
                </p>
                <div>
                  <p>from ${product.price.toFixed(2)}</p>
                  <img src={RightIcon} alt="Right icon"/>
                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    )
  }
}

const mapStateToProps = (state: any): any => {
  return {
    data: state.products.products,
  }
}

export default connect(mapStateToProps, null)(ProductsList)
