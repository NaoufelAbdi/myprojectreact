import React from 'react'
import SearchIcon from '../../assets/icons/search.png'
import BreadIcon from '../../assets/icons/bread.png'
import CloseIcon from '../../assets/icons/cross-white.png'
import CategoryFilter from '../../interfaces/CategoryFilter'
import HorizontalLine from '../HorizontalLine'

interface Props {
  onClose(): void,
  categoryFiltersData: CategoryFilter[]
  selectedCategoryFilters: CategoryFilter[]
  onToggleCategoryFilter(category: CategoryFilter): void
  visible: boolean
}

class RightSideBar extends React.Component<Props> {
  render(): React.ReactNode {
    const {
      visible,
      categoryFiltersData,
      selectedCategoryFilters,
      onToggleCategoryFilter,
    } = this.props
    return (
      <section className={`sidebar sidebar-right ${visible ? 'open' : ''}`}>
        <div className="content">
        <div className={'search-bar'}>
          <input placeholder={'Search for...'} autoComplete={'off'}/>
          <div className={'icon'}>
            <img src={SearchIcon} alt={'Search Icon'}/></div>
        </div>
        <div>
          <div className={'filters'}>
            <p className={'filter-label'}>FILTER</p>
            <HorizontalLine/>
            {categoryFiltersData.map((category, index) => {
              const isActive = selectedCategoryFilters.includes(category)
              return (
                <div
                  key={index}
                  className={`filter-item ${
                    isActive ? 'active' : ''
                  }`}
                  onClick={() => {
                    onToggleCategoryFilter(category)
                  }}
                >
                  <div className={'circle-icon'}>
                    <div className={'icon'}>
                      <img src={BreadIcon} alt={'Bread icon'}/>
                    </div>
                  </div>
                  <p className={'label'}>{category.name}</p>
                  <img
                    src={CloseIcon}
                    alt="Close filter"
                    className={'close-icon'}
                  />
                </div>
              )
            })}
          </div>
        </div>
        </div>
      </section>
    )
  }
}

export default RightSideBar
