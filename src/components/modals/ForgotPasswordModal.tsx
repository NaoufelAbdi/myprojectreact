import React from 'react'
import ProfileIcon from '../../assets/icons/profile.png'
import CloseTag from '../CloseTag'

interface Props {
  onClose?(): void
  visible: boolean
}
class ForgotPasswordModal extends React.Component<Props> {
  render(){
    const {onClose, visible} = this.props;
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body'}>
          <div className={'header'}>
            <CloseTag onClose={onClose}/>
            <img src={ProfileIcon} alt={'Profile'} className={'main-icon'}/>
            <h3 className={'title'}>LOGIN TO YOUR ACCOUNT</h3>
          </div>
        </div>
      </div>
    )
  }
}

export default ForgotPasswordModal
