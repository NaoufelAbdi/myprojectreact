import React from 'react'
import TickIcon from '../../assets/icons/tick-white.png'

interface Props {
  visible: boolean
  close(): void
}
interface State {
}

class ItemAddedModal extends React.Component<Props, State> {
  componentDidMount = (): void => {
    setTimeout(()=>{this.props.close()},3000);
  }
  render(): React.ReactNode {
    const {
      visible,
    } = this.props
    return (
      <div className={`added-modal ${visible ? 'open' : ''}`}>
          <div className={'body'}>
            <div className={'green-icon'}>
              <img src={TickIcon} alt={'tick icon'}/>
            </div>
            <h2>Item Added!</h2>
        </div>
      </div>
    )
  }
}

export default ItemAddedModal
