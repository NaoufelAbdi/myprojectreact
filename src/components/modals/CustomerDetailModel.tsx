import React from 'react'
import CloseTag from '../CloseTag'
import HorizontalLine from '../HorizontalLine'
import TickIcon from '../../assets/icons/tick-white.png'
import PersonIcon from '../../assets/icons/person.png'
import ClockIcon from '../../assets/icons/clock-primary.png'
import VerticalLine from '../VerticalLine'
import { connect } from 'react-redux'
import { Register } from '../../redux/user/types'
import { checkoutRegisterUser } from '../../redux/user/actions'
import { withTranslation, WithTranslation } from 'react-i18next'
import Loader from 'react-loader-spinner'
import { ValidateEmail, ValidatePhone } from '../../helpers/validator'
import moment from "moment";

interface Props extends WithTranslation {
  onSubmitClick?(): void

  onClose?(): void

  visible: boolean
  auth?: any
  Signup: (user: Register) => void
}

interface State {
  formErrors: any[];
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phone: string;
  confirmPassword: string;
  comparePassword: boolean
  login: string;
  toggle: boolean;
  firstNameError: boolean;
  lastNameError: boolean;
  emailError: boolean;
  passwordError: boolean;
  phoneError: boolean;
  confirmPasswordError: boolean;
}

class CustomerDetailModel extends React.Component<Props, State> {
  constructor(props: any) {
    super(props)
    this.state = {
      formErrors: [],
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      phone: '',
      login: '',
      confirmPassword: '',
      comparePassword: false,
      toggle: false,
      firstNameError: false,
      lastNameError: false,
      emailError: false,
      passwordError: false,
      phoneError: false,
      confirmPasswordError: false,
    }
  }

  onChangeHandler = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      [e.target.name]: e.target.value,
      firstNameError: false,
      lastNameError: false,
      emailError: false,
      passwordError: false,
      phoneError: false,
      confirmPasswordError: false,
    } as Pick<State, any>)
  }

  loginHandler = () => {
    const { login, firstName, lastName, confirmPassword, phone, email, password } = this.state
    const formErrors: any[] = []
    if (login === '') formErrors.push('username')
    if (firstName === '') formErrors.push('firstName')
    if (lastName === '') formErrors.push('lastName')
    if (email === '') formErrors.push('email')
    else if(!ValidateEmail(email)) formErrors.push('wrongEmail')
    if (phone === '') formErrors.push('phone')
    else if(!ValidatePhone(phone)) formErrors.push('wrongPhone')
    if (password === '') formErrors.push('password')
    if (confirmPassword === '') formErrors.push('confirmPassword')
    else if (password !== confirmPassword) formErrors.push('match')
    if (formErrors.length === 0) {
      this.props.Signup({
        login,
        email,
        password,
      })
    }
    this.setState({ formErrors })
  }

  onToggle = () => {
    this.setState({
      toggle: !this.state.toggle,
    })
  }

  formErrorMessage = (field) => {
    if (this.state.formErrors.length > 0 && this.state.formErrors.includes(field)) {
      const errorCheck = ()=>{
        if (field === 'match') return 'Please enter the same value again.'
        else if (field === 'wrongEmail') return 'Please enter a valid email address'
        else if (field === 'wrongPhone') return 'Please enter a valid phone number'
        else return 'This field is required.'
      }
      return (
        <h4 className={'alert-text'}>
          {errorCheck()}
        </h4>
      )
    }
  }

  render(): React.ReactNode {
    const {
      onClose,
      visible,
    } = this.props
    const { t } = this.props
    const { loadingCheckout } = this.props.auth
    const { login, firstName, lastName, email, phone, password, confirmPassword, emailError, firstNameError, lastNameError, phoneError, confirmPasswordError, passwordError, comparePassword } = this.state
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body lg'}>
          <div className={'white-header ph'}>
            <CloseTag onClose={onClose}/>
            <p className={'def'}>{t('YOUR ORDER WILL BE READY FOR PICKUP AT')}</p>
            <h3 className={'title'}>PENNE BOLOGNESE</h3>
            <p className={'sub'}>Atlassima adress Groupe</p>
            <HorizontalLine/>
          </div>
          <div className={'content'}>
            <div className={'details'}>
              <div className={'part'}>
                <div className={'partDetailsTime'}>
                  <img src={ClockIcon}/>
                  <h3>{t('Ready Time')}</h3>
                </div>
                <div className={'partDetailsTime'}>
                  <h2>{moment().add(20,'minutes').format("hh:mm:ss")}</h2>
                  <p>{moment().format("DD-MM-YYYY")}</p>
                </div>
              </div>
              <VerticalLine/>
              <div className={'part'}>
                <img src={PersonIcon}/>
                <h3>{t('Customer Details')}</h3>
                <div className="field-padding">

                  <div className="field-container">
                    <div>
                      {this.formErrorMessage('username')}
                      <input type="text" placeholder={t('Username')} name="login"
                             className={'no-margin'}
                             value={login}
                             onChange={this.onChangeHandler}/>
                    </div>
                  </div>
                  <div className="field-container">
                    <div>
                      {this.formErrorMessage('firstName')}
                      <input type="text" placeholder={t('First Name*')} name="firstName"
                             className={'no-margin'}
                             value={firstName}
                             onChange={this.onChangeHandler}/>
                    </div>
                  </div>
                  <div className="field-container">
                    <div>
                      {this.formErrorMessage('lastName')}
                      <input type="text" placeholder={t('Last Name*')} name="lastName"
                             value={lastName}
                             onChange={this.onChangeHandler}
                             className={'no-margin'}/>
                    </div>
                  </div>
                  <div className="field-container">
                    <div>
                      {this.formErrorMessage('email')}
                      {this.formErrorMessage('wrongEmail')}
                      <input type="text" placeholder={t('Email*')} name="email"
                             value={email}
                             onChange={this.onChangeHandler}
                             className={'no-margin'}/>
                    </div>
                  </div>
                  <div className="field-container">
                    <div>
                      {this.formErrorMessage('phone')}
                      {this.formErrorMessage('wrongPhone')}
                      <input type="text" placeholder={t('Phone*')} name="phone"
                             value={phone}
                             onChange={this.onChangeHandler}
                             className={'no-margin'}/>
                    </div>
                  </div>
                  <div>
                    <div className="field-container">
                      <div>
                        {this.formErrorMessage('password')}
                        <input type="password" placeholder={t('Password*')} name="password"
                               value={password}
                               onChange={this.onChangeHandler}
                               className={'no-margin'}/>
                      </div>
                    </div>
                    <div className="field-container">
                      <div>
                        {this.formErrorMessage('confirmPassword')}
                        {this.formErrorMessage('match')}
                        <input type="password" placeholder={t('Confirm Password*')} name="confirmPassword"
                               value={confirmPassword}
                               onChange={this.onChangeHandler}
                               className={'no-margin'}/>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <input type="text"
                   placeholder={t('You May Leave')}/>
          </div>
          <div className={'footer'}>
            <div className={'right'} onClick={this.loginHandler}>
              {loadingCheckout ? <div style={{ margin: '10px' }}><Loader
                  type="Oval"
                  color="white"
                  height={18}
                  width={18}
                  timeout={700000}
                /></div> :
                <h3>{t('Next')}</h3>
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}


const comp = connect((state: any) => {
  return {
    auth: state.auth,
    errorMessage: state.auth.errorMessage || false,
  }
}, (dispatch: any) => {
  return {
    Signup: (create: Register) => dispatch(checkoutRegisterUser(create)),
  }
})(CustomerDetailModel)

export default withTranslation()(comp)
