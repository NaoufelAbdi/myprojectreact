import React from 'react'
import CloseTag from '../CloseTag'
import TickIcon from '../../assets/icons/tick-white.png'
import VerticalLine from '../VerticalLine'
import { withTranslation, WithTranslation } from "react-i18next";
import { connect } from 'react-redux'
import { startNewOrderAction } from '../../redux/cart/actions'
import { Link } from 'react-router-dom'

interface Props extends WithTranslation {
  onClose?(): void
  newOrder?: any
  visible: boolean
  orderResponse?: any
}
interface State {
  quantity: number
}

class OrderSubmittedModal extends React.Component<Props, State> {
  handleStartNew = () => {
    this.props.onClose()
  }
  render(): React.ReactNode {
    const {
      onClose,
      visible,
    } = this.props
    const { t } = this.props
    const { id } = this.props.orderResponse
    return (
      <div className={`submitted-modal ${visible ? 'open' : ''}`}>
        <div className={'card  md'}>
          <div className={'body  md'}>
            <div className={'submitted-header ph'}>
              <CloseTag onClose={onClose} />
              <div className={'green-icon'}>
                <img src={TickIcon} alt={'tick icon'} />
              </div>
              <h3 className={'title'}>{t('Order Submitted')}</h3>
            </div>
            <div className={'info'}>
              <div className={'details'}>
                <h4>{t('ORDER NUMBER')}</h4>
                <h2>#{id}</h2>
              </div>
              <VerticalLine />
              <div className={'details'}>
                <h4>{t('ESTIMATED TIME')}</h4>
                <h2>03:24 AM</h2>
              </div>
            </div>
            <div className={'content'}>
              <div className={'details'}>
                <p>{this.props.t('An email confirmation will be sent to email')}</p>
                <p>{this.props.t('Need help with anything? Call 0643578216')}</p>
                <p className={'direction'}>{this.props.t('I need directions to the restaurant')}</p>
              </div>
            </div>
          </div>
          <div className={'footer'}>

            <div className={'right-2'} >
              <Link style={{ color: 'white', textDecoration: 'none' }} to={'account'}>
                <h3>{t('View Past Orders')}</h3>
              </Link >
            </div>
            <div className={'right'} onClick={this.handleStartNew}>
              <h3>{t('Start new Order')}</h3>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const comp = connect((state: any) => {
  return {
    orderResponse: state.cart.orderResponse
  };
})(OrderSubmittedModal);

export default withTranslation()(comp);
