import React from 'react'
import CloseTag from '../CloseTag'
import HorizontalLine from '../HorizontalLine'
import { connect } from 'react-redux'
import { withTranslation, WithTranslation } from "react-i18next";
import { Redirect } from 'react-router-dom'

interface Props extends WithTranslation {
    onClose?(): void
    visible: boolean
    orderDetail?: any
}
interface State {
    reOrder: boolean
}
class OrderDetailModel extends React.Component<Props, State> {
    constructor(props: any) {
        super(props)
        this.state = {
            reOrder: false
        }
    }
    handleReorder = () => {
        this.setState({ reOrder: true })
    }
    render() {
        const {
            onClose,
            visible,
        } = this.props
        const { t } = this.props
        if (this.state.reOrder) {
            return (<Redirect to={"/"} />);
        }
        const order = this.props.orderDetail || []
        let Items
        if(order.length === 0)
             Items = []
        else{
            Items = order.items
        }
        return (
            <div className={`orderDetailsModal ${visible ? 'open' : ''}`}>
                <div className={'body  md'}>
                    <div className={'white-header ph'}>
                        <CloseTag onClose={onClose} />
                        <div className={"dateData"}>
                            <p>Date: <span>{order.date? order.date : "_"}</span></p>
                            <div className={"preparingOrderDetail"}>
                                <h2>#{order.id}</h2>
                                <p>{this.props.t('Preparing')}</p>
                            </div>
                            <h5>{this.props.t('Tax Invoice')}</h5>
                            <div className={"orderDetailsTimeDate"}>
                                <p>{this.props.t('for')}</p>
                                <p className={"orderTimeDataP"} >{order.date? order.date:  "_"}</p>
                                <p>{this.props.t('at')}</p>
                                <p className={"orderTimeDataP"} >{order.time? order.time: "_"}</p>
                            </div>
                            <div className={"orderDetailsLittleItaly"}>
                                <p>{this.props.t('from')}</p>
                                <p className={"orderTimeDataP"} >{this.props.t('Atlassima group')}</p>
                            </div>
                        </div>
                        <HorizontalLine />
                        <div className={"orderDetailSection"}>
                            <h2>{this.props.t('your order')}</h2>
                            <div className={"orderDetailsCart"}>
                                <HorizontalLine />
                                {Items.map((item , index) => {
                                    return (
                                      <div className={'item'}>
                                          <p>{"1"} x {item.itemName}</p>
                                          <p>{item.price}</p>
                                      </div>
                                    )
                                })}
                                <HorizontalLine />
                                <div className={"orderDetailTotalCart"}>
                                    <p>{this.props.t('Total')}</p>
                                    <p>${order.totalAmount}</p>
                                </div>
                                <HorizontalLine />
                                <div>
                                    <p>{this.props.t('special instruction')}</p>
                                </div>
                                <HorizontalLine />
                                <div>
                                    <p>{this.props.t('Payment Type')}</p>
                                </div>
                                <div>
                                    <p className={'orderDetailCode'}>Code</p>
                                </div>
                                <HorizontalLine />
                            </div>
                            <h2>RESTAURANT</h2>
                            <div className={"tableOrderDetail"}>
                                <h3>{this.props.t('Atlassima group')}</h3>
                                <div>
                                    <h2>{this.props.t('address')}:</h2>
                                    <p>{order.address? order.address : "_"}</p>
                                </div>
                                <div>
                                    <h2>phone:</h2>
                                    <p>{order.phoneNumber? order.phoneNumber : "_"}</p>
                                </div>
                                <div>
                                    <h2>ABN:</h2>
                                    <p>{order.abn? order.abn : "__"}</p>
                                </div>
                            </div>
                            <h2>{this.props.t('CUSTOMER')}</h2>
                            <div className={"tableOrderDetail"}>
                                <HorizontalLine />
                                <div>
                                    <h2>{this.props.t('name')}:</h2>
                                    <p>{order.customerName? order.customerName : "_"}</p>
                                </div>
                                <div>
                                    <h2>e-mail:</h2>
                                    <p>{order.email? order.email : "_"}</p>
                                </div>
                                <div>
                                    <h2>phone:</h2>
                                    <p>{order.phoneNumber? order.phoneNumber: "_"}</p>
                                </div>
                            </div>
                        </div>
                        <HorizontalLine />
                    </div>
                    <div className={'footer'} onClick={this.handleReorder}>
                        <div className={'right'} >
                            <h3>{this.props.t('re-order')}</h3>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

const comp = connect((state: any) => {
    return {
        orderDetail: state.order.orderClick
    };
}, (dispatch: any) => {
    return {

    };
})(OrderDetailModel);


export default withTranslation()(comp);
