import React from 'react'
import CloseTag from '../CloseTag'
import HorizontalLine from '../HorizontalLine'
import TickIcon from '../../assets/icons/tick-white.png'
import CashIcon from '../../assets/icons/cash.png'
import { connect } from 'react-redux'
import { withTranslation, WithTranslation } from "react-i18next";
import * as types from '../../redux/user/types'
import { orderData } from '../../redux/cart/types'
import { orderRequestAction } from '../../redux/cart/actions'
import {checkOutConfigAction , paymentMethodAction} from '../../redux/orders/actions'
import Loader from 'react-loader-spinner'
import {FaRegCreditCard} from 'react-icons/fa'

interface Props extends WithTranslation {
  oderRequest?(totalData: orderData): void
  onSubmitClick?(): void
  onClose?(): void
  category?: any
  total?: any
  loading?: boolean
  userInfo: types.UserInfo
  isOrderSubmit?: boolean
  visible: boolean
  checkOutConfig?(): any
  paymentModel?(): void
  paymentMethods?(): void
}

interface State {
  paymentMethod: string;
}

class CheckoutModal extends React.Component<Props, State> {
  state={
    paymentMethod: ''
  }
  componentWillReceiveProps(props: any) {
    if (props.isOrderSubmit && this.props.isOrderSubmit !== props.isOrderSubmit) {
      this.props.onSubmitClick()
    }
  }

  onOrderSubmitClick = () => {
    const { grandTotal, cart, } = this.props.total
    const { email } = this.props.userInfo
    let storeId = cart[0].product.category.store.id;
    const orderData = {
      storeId: storeId,
      email: email,
      totalAmount: grandTotal.grandTotal,
      orderStatus: "pending",
      items: grandTotal.items
    }
    this.props.oderRequest(orderData)
  }

  setPaymentMethod = (paymentMethod) =>{
    this.setState({paymentMethod})
  }

  handlePaymentModel = () => {
    this.props.checkOutConfig()
    this.props.paymentMethods()
    this.props.paymentModel()
  }

  render() {
    const {
      onClose,
      visible,
    } = this.props
    const loading = this.props.loading
    const { t } = this.props
    const { grandTotal , cart } = this.props.total
    console.log("data is " , cart)
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'body  md'}>
          <div className={'white-header ph'}>
            <CloseTag onClose={onClose} />
            <p className={'def'}>{t('YOUR ORDER WILL BE READY FOR PICKUP AT')}</p>
            <h3 className={'title'}>Atlassima Restaurant</h3>
            <p className={'sub'}>Atlasssima groupe adresse</p>
            <HorizontalLine />
          </div>
          <div className={'content'}>
            <div className={'payment'}>
              <div className={'method'} onClick={()=>this.setPaymentMethod('Cash')}>
                <div className={'cash-icon'}>
                  <img src={CashIcon} alt={'cash icon'} />
                </div>
                <h4>{t('Cash')}</h4>
                <div className={this.state.paymentMethod==='Cash'?'green-icon':'green-icon non-selected'}>
                  <img src={TickIcon} alt={'tick icon'} />
                </div>
              </div>
              <div className={'method'} onClick={()=>this.setPaymentMethod('Card')}>
                <div className={'cash-icon'}>
                  <FaRegCreditCard style={{color: "white"}}/>
                </div>
                <h4>Card</h4>
                <div className={this.state.paymentMethod==='Card'?'green-icon':'green-icon non-selected'}>
                  <img src={TickIcon} alt={'tick icon'} />
                </div>
              </div>
              <h2>{t('Total')} ${grandTotal.grandTotal}</h2>
            </div>
          </div>
          <div className={'footer'}>
            {this.state.paymentMethod===''?(
              <div className={'right off'}>
                <h3>{t('Select Payment Method')}</h3>
              </div>
            ):this.state.paymentMethod === 'Cash'? (
              <div className={'right'} onClick={this.onOrderSubmitClick}>
                {loading ? <div style={{margin: "10px"}}><Loader
                    type="Oval"
                    color="white"
                    height={18}
                    width={18}
                    timeout={700000}
                  /> </div> :
                  <h3>{t('Submit Payment')}</h3>
                }
              </div>
              ): (
              <div className={'right'} onClick={this.handlePaymentModel}>
                  <h3>{t('Next')}</h3>
              </div>
              )
            }
          </div>
        </div>
      </div>
    )
  }
}

const comp = connect((state: any) => {
  return {
    total: state.cart,
    userInfo: state.auth.userInfo,
    loading: state.cart.loading,
    isOrderSubmit: state.cart.isOrderSubmit
  };
}, (dispatch: any) => {
  return {
    oderRequest: (totalData: orderData) => dispatch(orderRequestAction(totalData)),
    checkOutConfig: () => dispatch(checkOutConfigAction()),
    paymentMethods: () => dispatch(paymentMethodAction()),
  };
})(CheckoutModal);

export default withTranslation()(comp);
