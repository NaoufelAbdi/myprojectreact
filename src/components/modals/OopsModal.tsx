import React, { Component } from 'react'
import { WithTranslation } from 'react-i18next'

interface Props {
  onClose?(): void
  errorMessage?: string
  visible: boolean
}

class OopsModal extends React.Component<Props> {
  render(): React.ReactNode {
    const {
      onClose,
      visible,
    } = this.props
    return (
      <div className={`modal ${visible ? 'open' : ''}`}>
        <div className={'hotsale-card'}>
          <div className={'oops-header'}>
            <img src={require('../../assets/icons/error-round.png')}/>
            <h3 className={'title'}>OOPS!</h3>
          </div>
          <div className={'details'}>
            <p>{this.props.errorMessage}</p>
            <div className={'ok-button'} onClick={onClose}>
              <p>OK</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default OopsModal
