import React from 'react'
import { withRouter } from 'react-router-dom'
import { User } from '../redux/user/types'

interface Props {
  children: React.ReactNode,
  history?: any,
  location?: any,
  match?: any
  user?: User
}

class Page extends React.Component<Props> {
  componentDidMount() {
    this.validateUser()
  }
  componentDidUpdate() {
    this.validateUser()
  }
  validateUser = () => {
    // if (this.props.user && this.props.user.uid.length === 0 && this.props.location.pathname !== '/') {
    //   console.log(this.props.location.pathname)
    //   this.props.history.replace('/')
    // }
  }

  render(): React.ReactNode {
    return <div className={'page'}>{this.props.children}</div>
  }
}

// @ts-ignore
export default withRouter(Page)
