import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import Home from './screens/Home'
import Account from './screens/Account'
import { withTranslation, WithTranslation} from 'react-i18next'

class AppRouter extends React.Component<WithTranslation> {
  render(): React.ReactNode {
    return (
      <Router>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
          <Route path="/account" exact>
            <Account />
          </Route>
        </Switch>
      </Router>
    )
  }
}

export default  withTranslation()(AppRouter)