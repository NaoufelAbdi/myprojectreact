import { types , OrdersState} from "./types";

const initialState: OrdersState = {
  orders: [],
  orderClick: [],
  reOrder: [],
  CheckOutConfig: "",
  PaymentMethods: ""
}

const orderReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case types.LOAD_ORDERS_SUCCESS:
      return {
        ...state,
        orders: action.payload
      };
    case types.CLICK_ORDER_REQUEST:
      return {
        ...state,
        orderClick: action.payload
      };
    case types.CLICK_REORDER_REQUEST:
      return {
        ...state,
        reOrder: action.payload
      };
    case types.CHECK_OUT_CONFIG_SUCCESS:
      return {
        ...state,
        CheckOutConfig: action.payload
      };
    case types.CHECK_OUT_CONFIG_FAILURE:
      return {
        ...state,
        CheckOutConfig: ""
      };
    case types.GET_PAYMENT_METHODS_SUCCESS:
      return {
        ...state,
        PaymentMethods: action.payload
      };
    case types.GET_PAYMENT_METHODS_FAILURE:
      return {
        ...state,
        PaymentMethods: ""
      };
    default:
      return state
  }
}

export default orderReducer
