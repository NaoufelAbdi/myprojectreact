
import { takeLatest, call, put, select } from 'redux-saga/effects'
import { types } from './types';
import * as services from './service'
import { selectToken } from '../selectors/selectors'


function* OrdersGet(action: any) {
    try {
        const response = yield call(services.callGetOrder, action);
        yield put({
            type: types.LOAD_ORDERS_SUCCESS,
            payload: response.data
        })
    } catch (error) {
    }
}
function* CheckOutConfigSaga() {
    try {
        let token = yield select(selectToken);
        const response = yield call(services.callCheckOutConfig , token);
        console.log("checkout-config =>" , response)
        yield put({
            type: types.CHECK_OUT_CONFIG_SUCCESS,
            payload: response.data
        })
    } catch (error) {
        yield put({
            type: types.CHECK_OUT_CONFIG_FAILURE
        })
    }
}
function* GetPaymentMethodSaga() {
    try {
        let token = yield select(selectToken);
        const response = yield call(services.callPaymentMethod , token);
        console.log("payment-method =>" , response.data.paymentMethods)
        yield put({
            type: types.GET_PAYMENT_METHODS_SUCCESS,
            payload: response.data.paymentMethods[0]
        })
    } catch (error) {
        yield put({
            type: types.GET_PAYMENT_METHODS_FAILURE
        })
    }
}

export default function* orderWatcher() {
    yield takeLatest(types.LOAD_ORDERS_REQUEST, OrdersGet);
    yield takeLatest(types.CHECK_OUT_CONFIG_REQUEST, CheckOutConfigSaga);
    yield takeLatest(types.GET_PAYMENT_METHODS_REQUEST , GetPaymentMethodSaga);
}
