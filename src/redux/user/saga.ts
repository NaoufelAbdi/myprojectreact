import { takeLatest, call, put , select} from 'redux-saga/effects';
import { types } from './types';
import * as services from './service'
import {selectToken} from '../selectors/selectors'

function* LoginSaga(action: any) {
    try {
        const response = yield call(services.callLogin, action);
        if (response.status === 200) {
            yield put({
                type: types.LOGIN_SUCCESS,
                payload: response.data
            });
            try {
                const token = response.data.id_token
                const infoResponse =  yield call(services.callUserInfo , token)
                yield put({
                    type: types.USER_INFO_SUCCESS,
                    payload: infoResponse.data
                })
            }
            catch (err){
                yield put({
                    type: types.USER_INFO_FAILURE,
                });
            }
        }
        else {
            yield put({
                type: types.LOGIN_FAILURE,
            });
        }
    } catch (error) {
        yield put({
            type: types.LOGIN_FAILURE,
            payload: error.response.data.detail
        })
    }
}

function* RegisterSaga(action: any) {
    try {
        const response = yield call(services.callRegister, action);
        if (response.status === 201) {
            yield put({
                type: types.REGISTER_SUCCESS,
                payload: response,
            });
        }
    } catch (error) {
        // if (error.response && error.response.status === 400) {
        yield put({
            type: types.REGISTER_FAILURE,
            payload: error.response.data.title,
        });
    }
}
function* CheckOutRegisterSaga(action: any) {
    try {
        const response = yield call(services.callRegister, action);
        if (response.status === 201) {
            yield put({
                type: types.REGISTER_ON_CHECKOUT_SUCCESS,
                payload: response,
            });
        }
    } catch (error) {
        yield put({
            type: types.REGISTER_ON_CHECKOUT_FAIL,
            payload: error.response.data.title,
        });
    }
}
function* LogoutUser(action: any) {
    try {
        yield put({
            type: types.LOGOUT_SUCCESS
        })
    } catch (error) {

    }
}
function* PasswordChangeSaga(action: any) {
    try {
        let token = yield select(selectToken);
        const response = yield call(services.callPasswordChange, action , token);
        if (response.status === 200) {
            yield put({
                type: types.CHANGE_PASSWORD_SUCCESS,
            });
        }
    } catch (error) {
        yield put({
            type: types.CHANGE_PASSWORD_FAILURE,
        });
    }
}
function* UserUpdateSaga(action: any) {
    try {
        let token = yield select(selectToken);
        const response = yield call(services.callUserUpdate, action , token);
        if (response.status === 200) {
            yield put({
                type: types.USER_UPDATE_SUCCESS,
            });
            try{
                const infoResponse =  yield call(services.callUserInfo , token)
                yield put({
                    type: types.USER_INFO_SUCCESS,
                    payload: infoResponse.data
                })
            }
            catch(err){
                yield put({
                    type: types.USER_INFO_FAILURE,
                });
            }
        }
    } catch (error) {
        yield put({
            type: types.USER_UPDATE_FAILURE,
        });
    }
}

export default function* loginWatcher() {
    yield takeLatest(types.LOGIN_REQUEST, LoginSaga);
    yield takeLatest(types.REGISTER_REQUEST, RegisterSaga);
    yield takeLatest(types.REGISTER_ON_CHECKOUT_REQUEST, CheckOutRegisterSaga);
    yield takeLatest(types.LOGOUT_REQUEST, LogoutUser);
    yield takeLatest(types.CHANGE_PASSWORD_REQUEST, PasswordChangeSaga);
    yield takeLatest(types.USER_UPDATE_REQUEST, UserUpdateSaga);
}
