const LOGIN_REQUEST: string = 'LOGIN_REQUEST'
const LOGIN_SUCCESS: string = 'LOGIN_SUCCESS'
const LOGIN_FAILURE: string = 'LOGIN_FAILURE'
const USER_INFO_FAILURE: string = 'USER_INFO_FAILURE'
const USER_INFO_SUCCESS: string = 'USER_INFO_SUCCESS'
const REGISTER_REQUEST: string = 'REGISTER_REQUEST'
const REGISTER_SUCCESS: string = 'REGISTER_SUCCESS'
const REGISTER_FAILURE: string = 'REGISTER_FAILURE'
const LOGOUT_REQUEST: string = 'LOGOUT_REQUEST'
const LOGOUT_SUCCESS: string = 'LOGOUT_SUCCESS'
const REMOVE_ERROR: string = 'REMOVE_ERROR'
const REGISTER_ON_CHECKOUT_REQUEST: string = 'REGISTER_ON_CHECKOUT_REQUEST'
const REGISTER_ON_CHECKOUT_SUCCESS: string = 'REGISTER_ON_CHECKOUT_SUCCESS'
const REGISTER_ON_CHECKOUT_FAIL: string = 'REGISTER_ON_CHECKOUT_FAIL'
const CHANGE_PASSWORD_REQUEST: string = 'CHANGE_PASSWORD_REQUEST'
const CHANGE_PASSWORD_SUCCESS: string = 'CHANGE_PASSWORD_SUCCESS'
const CHANGE_PASSWORD_FAILURE: string = 'CHANGE_PASSWORD_FAILURE'
const USER_UPDATE_REQUEST: string = 'USER_UPDATE_REQUEST'
const USER_UPDATE_SUCCESS: string = 'USER_UPDATE_SUCCESS'
const USER_UPDATE_FAILURE: string = 'USER_UPDATE_FAILURE'

export const types = {
  LOGIN_REQUEST,
  LOGIN_SUCCESS,
  LOGIN_FAILURE,
  USER_INFO_SUCCESS,
  USER_INFO_FAILURE,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
  REGISTER_FAILURE,
  LOGOUT_REQUEST,
  LOGOUT_SUCCESS,
  REMOVE_ERROR,
  REGISTER_ON_CHECKOUT_REQUEST,
  REGISTER_ON_CHECKOUT_SUCCESS,
  REGISTER_ON_CHECKOUT_FAIL,
  CHANGE_PASSWORD_REQUEST,
  CHANGE_PASSWORD_SUCCESS,
  CHANGE_PASSWORD_FAILURE,
  USER_UPDATE_REQUEST,
  USER_UPDATE_SUCCESS,
  USER_UPDATE_FAILURE
}

export interface LoginState {
  loginUser?: LoginUser;
  success?: string;
  loading?: boolean;
  error?: boolean;
  errorMessage?: string;
  token?: string;
  loggedInStatus?: boolean;
  logOutStatus?: boolean;
  checkOutRegister?: boolean;
  userInfo? : string;
  changePassword? : boolean;
  registerStatus?: boolean;
  updateUser?: boolean;
  loadingCheckout?: boolean
}

export interface User {
  username: string
  password: string
}

export interface PasswordInterface {
  currentPassword: string,
  newPassword: string,
}
export interface UpdateInterface {
  firstName: string,
  lastName: string,
  phone?: string,
  activated: boolean,
  authorities: any,
  createdBy: string,
  reatedDate: string,
  email: string,
  id: number,
  imageUrl: string,
  langKey: string,
  lastModifiedBy: string,
  lastModifiedDate: string,
  login: string
}

export interface UserInfo {
  id: number,
  login: string,
  firstName: string,
  lastName: string,
  email: string,
  imageUrl: string,
  activated: boolean,
  langKey: string,
  createdBy: string,
  createdDate: string,
  lastModifiedBy: string,
  lastModifiedDate: string,
  authorities: any,
}
export interface Register {
  login: string
  email: string
  password: string
}
export interface CheckOutRegister {
  login: string
  email: string
  password: string
}

export interface LoginUser {
  type: typeof LOGIN_REQUEST
  payload: User
}

export interface RegisterUser {
  type: typeof REGISTER_REQUEST
  payload: Register
}

export interface CheckOutRegisterUser {
  type: typeof REGISTER_ON_CHECKOUT_REQUEST
  payload: CheckOutRegister
}

export interface LogoutUser {
  type: typeof LOGOUT_REQUEST
}

export interface ChangePasswordAC {
  type: typeof CHANGE_PASSWORD_REQUEST
  payload: PasswordInterface
}
export interface UserUpdateAc {
  type: typeof CHANGE_PASSWORD_REQUEST
  payload: UpdateInterface
}
export type UserActionTypes = LoginUser | LogoutUser | CheckOutRegisterUser | ChangePasswordAC |  UserUpdateAc
