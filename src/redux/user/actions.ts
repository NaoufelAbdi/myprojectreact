import { types, LoginUser, RegisterUser, LogoutUser, User, Register , UserUpdateAc, PasswordInterface , UpdateInterface,ChangePasswordAC} from './types'

export const loginUser = (user: User): LoginUser => {
  return {
    type: types.LOGIN_REQUEST,
    payload: user,
  }
}


export const registerUser = (user: Register): RegisterUser => {
  return {
    type: types.REGISTER_REQUEST,
    payload: user,
  }
}

export const checkoutRegisterUser = (user: Register): RegisterUser => {
  return {
    type: types.REGISTER_ON_CHECKOUT_REQUEST,
    payload: user,
  }
}

export const logoutUser = (): LogoutUser => {
  return {
    type: types.LOGOUT_REQUEST,
  }
}

export const remove = (): any => {
  return {
    type: types.REMOVE_ERROR,
    payload: "",
  }
}

export const passwordChange = (passwordData: PasswordInterface): ChangePasswordAC => {
  return {
    type: types.CHANGE_PASSWORD_REQUEST,
    payload: passwordData,
  }
}

export const userUpdate = (update: UpdateInterface): UserUpdateAc => {
  return {
    type: types.USER_UPDATE_REQUEST,
    payload: update,
  }
}
