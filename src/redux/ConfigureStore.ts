import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
import { fork, all } from 'redux-saga/effects';
import userReducer from './user/reducer'
import cartReducer from './cart/reducer'
import productReducer from './products/reducer'
import orderReducer from './orders/reducer'
import loginWatcher from './user/saga'
import productWatcher from './products/saga';
import cartWatcher from './cart/saga';
import orderWatcher from './orders/saga'

function* rootSaga() {
  yield all([
    fork(loginWatcher),
    fork(productWatcher),
    fork(cartWatcher),
    fork(orderWatcher)
  ]);
}
const sagaMiddleware = createSagaMiddleware();

const rootReducers = combineReducers({
  auth: userReducer,
  cart: cartReducer,
  products: productReducer,
  order: orderReducer
});

const persistConfig = {
  key: 'primary',
  storage,
  whitelist: ["auth", "products", "order"],
}

const persistedReducer = persistReducer(persistConfig, rootReducers)

// Middleware: Redux Persist Persisted Reducer
// const store = createStore(persistedReducer, compose(
//   applyMiddleware(sagaMiddleware),
//   (window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()
// ));

const store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
let persistor = persistStore(store);
sagaMiddleware.run(rootSaga);

export { store, persistor };
