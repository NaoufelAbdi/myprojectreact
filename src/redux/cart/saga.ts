
import { takeLatest, call, put } from 'redux-saga/effects';
import { types } from './types'
import * as services from './services';

function* getOrderRequestSaga(action: any) {
    try {
        const response = yield call(services.callOrderSubmit, action);
        if (response.status === 201) {

            yield put({
                type: types.ORDER_DATA_SUCCESS,
                payload: response.data
            });
        }
    }
    catch (error) {
        yield put({
            type: types.ORDER_DATA_FAILURE
        })
    }
}
export default function* cartWatcher() {
    yield takeLatest(types.ORDER_DATA_REQUEST, getOrderRequestSaga);
}