import {
  AddProductToCart,
  CartItem,
  GrandTotal,
  OrderData,
  orderData,
  RemoveProductFromCart,
  StartOerder,
  total,
  UpdateProductInCart,
  types
} from './types'

export const addProductToCart = (cartItem: CartItem): AddProductToCart => {
  return {
    type: types.ADD_PRODUCT_TO_CART,
    payload: cartItem,
  }
}
export const updateProductInCart = (cartItem: CartItem): UpdateProductInCart => ({
  type: types.UPDATE_PRODUCT_IN_CART,
  payload: cartItem,
})

export const removeProductFromCart = (cartID: number): RemoveProductFromCart => ({
  type: types.REMOVE_PRODUCT_FROM_CART,
  payload: cartID,
})

export const grandTotalAction = (totalData: total): GrandTotal => {
  return {
    type: types.GRAND_TOTAL_SAVE,
    payload: totalData
  }
}

export const orderRequestAction = (totalData: orderData): OrderData => {
  return {
    type: types.ORDER_DATA_REQUEST,
    payload: totalData
  }
}



export const startNewOrderAction = (): StartOerder => {
  return {
    type: types.START_NEW_ORDER,
  }
}
