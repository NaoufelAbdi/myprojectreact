
import { takeLatest, call, put } from 'redux-saga/effects';
import {types} from './types'
import * as services from './services';

function* getCategorySaga(action: any) {
    try {
        const response = yield call(services.callAllCategory, action);
        //    if (response.status === 200) {
        //        yield put({
        //            type: types.LOAD_CATEGORY_SUCCESS,
        //            payload: response.data.categories,
        //        });
        //    }
           yield put({
               type: types.LOAD_CATEGORY_SUCCESS,
              payload: response.categories,
           });
    } catch (error) {
        yield put({
            type: types.LOAD_CATEGORY_FAILURE
        })

    }
}

export default function* productWatcher() {
     yield takeLatest(types.LOAD_CATEGORY, getCategorySaga);
}